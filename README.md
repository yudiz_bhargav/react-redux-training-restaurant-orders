# React-Redux Training Project

# Responsive Online Restaurant Order Application (with a mobile first design)

## Users can select from different categories and their sub-categories to add to a basket, all the items they wish to devour.

### Cart Total will dynamically change and adjust to the number of food items, user has selected

#### Use of Redux, React Router, Hooks, PropTypes checking and more
