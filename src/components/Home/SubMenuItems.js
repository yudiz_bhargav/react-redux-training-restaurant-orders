/* eslint-disable prefer-destructuring */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import SubMenuItemsModal from '../Modals/SubMenuItemsModal';
import '../../CSS/Home-Page-Styles/SubMenuItemsStyles.css';

const SubMenuItems = (props) => {
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const [propsOfClickedProduct, setPropsOfClickedProduct] = React.useState({});

  const { products } = props;
  const { productsOfSubCategory } = useParams();
  const subCategoryId = parseInt(productsOfSubCategory.split('-')[1], 10);

  const filteredProductsAsPerSubCategory = products.filter((product) => {
    return product.parentId === subCategoryId;
  });

  const propsOfAllProductsArray = [];

  const filterPropsOfAllProductsArray = (productId) => {
    const res = propsOfAllProductsArray.filter((prod) => {
      return prod.id === productId;
    });
    setPropsOfClickedProduct(res[0]);
  };

  return (
    <>
      {isModalOpen && (
        <SubMenuItemsModal
          toggleModal={setIsModalOpen}
          {...propsOfClickedProduct}
        />
      )}
      <ul className="sub-menu-item-list-container">
        {filteredProductsAsPerSubCategory.map((product) => {
          const { id, parentId, name, description, price, variants, extras } =
            product;

          const obj = {
            id,
            parentId,
            name,
            description,
            price,
            variants,
            extras,
          };

          propsOfAllProductsArray.push(obj);

          return (
            <li
              key={id}
              className="sub-menu-item-container"
              onClick={() => {
                setIsModalOpen(true);
                filterPropsOfAllProductsArray(id);
              }}
            >
              <div>
                <h3 className="sub-menu-item-heading">{name}</h3>
                <p className="sub-menu-item-text">{description}</p>
              </div>
              <h2 className="sub-menu-item-price">£{price}</h2>
            </li>
          );
        })}
      </ul>
    </>
  );
};

const mapStateToProps = (state) => {
  const { products } = state.main;
  return {
    products,
  };
};

SubMenuItems.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      parentId: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      variants: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string,
          price: PropTypes.number,
        })
      ),
      extras: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string,
          price: PropTypes.number,
        })
      ),
    })
  ),
};

SubMenuItems.defaultProps = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      variants: null,
      extras: null,
    })
  ),
};

export default connect(mapStateToProps)(SubMenuItems);
