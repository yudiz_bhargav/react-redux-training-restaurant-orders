import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, NavLink, useParams } from 'react-router-dom';
import SubMenuItems from './SubMenuItems';
import '../../CSS/Home-Page-Styles/SubMenuCategoriesStyles.css';

import { setProducts } from '../../ReduxStuff/actionCreators';

const SubMenuCategories = ({ subCategories, dispatch }) => {
  const { category } = useParams();
  const categoryId = parseInt(category.split('-')[1], 10);

  const filteredSubCategories = subCategories.filter((subCategory) => {
    return subCategory.parent === categoryId;
  });

  return (
    <>
      <div className="sub-menu-categories-container">
        {filteredSubCategories.map((subCategory) => {
          const { id, name } = subCategory;
          return (
            <NavLink
              key={id}
              to={`/home/${category}/${name}-${id}`}
              className="sub-menu-category-item"
              activeClassName="sub-menu-category-item-active"
              onClick={() => {
                dispatch(setProducts(id));
              }}
            >
              {name}
            </NavLink>
          );
        })}
      </div>
      <Route path={`/home/${category}/:productsOfSubCategory`}>
        <SubMenuItems />
      </Route>
    </>
  );
};

const mapStateToProps = (state) => {
  const { subCategories, products } = state.main;
  return { subCategories, products };
};

SubMenuCategories.propTypes = {
  subCategories: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      parent: PropTypes.number,
    })
  ).isRequired,

  dispatch: PropTypes.func.isRequired,
};
export default connect(mapStateToProps)(SubMenuCategories);
