/* eslint-disable no-return-assign */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import '../../CSS/Home-Page-Styles/HomeFooterStyles.css';
import { useTranslation } from 'react-i18next';

import {
  setBasketTotalPrice,
  setNumberOfBasketItems,
} from '../../ReduxStuff/actionCreators';

const HomeFooter = ({ itemsInBasket, dispatch }) => {
  const { t } = useTranslation();

  if (itemsInBasket.length !== 0) {
    let grandTotal = 0;
    let totalNumberOfItemsInBasket = 0;
    itemsInBasket.map((item) => {
      let totalCheckboxPrice = 0;
      const { radioBtnValue, checkboxValue, counter } = item;
      checkboxValue.map((singleCheckboxItem) => {
        return (totalCheckboxPrice += singleCheckboxItem.price);
      });

      grandTotal += (radioBtnValue.variantPrice + totalCheckboxPrice) * counter;
      totalNumberOfItemsInBasket += 1;

      return item;
    });

    return (
      <div className="home-footer">
        <Link
          to="/checkout"
          onClick={() => {
            dispatch(setBasketTotalPrice(grandTotal));
            dispatch(setNumberOfBasketItems(totalNumberOfItemsInBasket));
          }}
        >
          <div className="home-footer-text-container">
            <h2 className="home-footer-text">{t('homeFooter.viewBasket')}</h2>
            <h2 className="home-footer-text">
              £{grandTotal.toFixed(2)} / {totalNumberOfItemsInBasket}{' '}
              {t('homeFooter.items')}
            </h2>
          </div>
        </Link>
      </div>
    );
  }
  return <></>;
};

const mapStateToProps = (state) => {
  const { itemsInBasket } = state.main;
  return { itemsInBasket };
};

HomeFooter.propTypes = {
  itemsInBasket: PropTypes.arrayOf(
    PropTypes.shape({
      currentOrderId: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired,
      parentId: PropTypes.number.isRequired,
      counter: PropTypes.number.isRequired,
      checkboxValue: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string,
          price: PropTypes.number,
        })
      ),
    })
  ),
  dispatch: PropTypes.func.isRequired,
};

HomeFooter.defaultProps = {
  itemsInBasket: PropTypes.arrayOf(
    PropTypes.shape({
      radioBtnValue: {},
    })
  ),
};

export default connect(mapStateToProps)(HomeFooter);
