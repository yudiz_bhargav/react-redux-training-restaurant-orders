import React from 'react';
import { useTranslation } from 'react-i18next';

const lngs = {
  en: { nativeName: 'English' },
  gj: { nativeName: 'ગુજરાતી' },
};

const HomeHeader = () => {
  const { t, i18n } = useTranslation();

  return (
    <>
      <div className="language-selector-container">
        {Object.keys(lngs).map((lng) => {
          return (
            <button
              key={lng}
              className="language-selector-buttons"
              style={{ fontWeight: i18n.language === lng ? 'bold' : 'normal' }}
              type="submit"
              onClick={() => {
                i18n.changeLanguage(lng);
              }}
            >
              {lngs[lng].nativeName}
            </button>
          );
        })}
      </div>
      <h1 className="home-header-title">{t('homeHeader.title')}</h1>
      <p className="home-header-location">{t('homeHeader.location')}</p>
    </>
  );
};

export default HomeHeader;
