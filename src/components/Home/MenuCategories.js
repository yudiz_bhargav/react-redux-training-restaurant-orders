import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, NavLink } from 'react-router-dom';
import SubMenuCategories from './SubMenuCategories';
import '../../CSS/Home-Page-Styles/MenuCategoriesStyles.css';

import { setSubCategories } from '../../ReduxStuff/actionCreators';

const MenuCategories = ({ primaryCategories, dispatch }) => {
  return (
    <>
      <div className="home-menu-categories-btn-container">
        {primaryCategories.map((category) => {
          const { id, name } = category;
          return (
            <NavLink
              key={id}
              to={`/home/${name}-${id}`}
              className="home-menu-categories"
              activeClassName="home-menu-categories-active"
              onClick={() => {
                dispatch(setSubCategories(id));
              }}
            >
              {name}
            </NavLink>
          );
        })}
      </div>
      <Route path="/home/:category">
        <SubMenuCategories />
      </Route>
    </>
  );
};

const mapStateToProps = (state) => {
  const { primaryCategories } = state.main;
  return { primaryCategories };
};

MenuCategories.propTypes = {
  primaryCategories: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    })
  ),

  dispatch: PropTypes.func.isRequired,
};

MenuCategories.defaultProps = {
  primaryCategories: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    })
  ),
};

export default connect(mapStateToProps)(MenuCategories);
