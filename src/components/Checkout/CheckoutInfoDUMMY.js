/* eslint-disable no-return-assign */
/* eslint-disable react/prop-types */
/* eslint-disable no-plusplus */
/* eslint-disable jsx-a11y/label-has-associated-control */

// WHOEVER REFERRING THROUGH THIS FILE, YOU MAY SKIP IT AND MOVE AHEAD WITH THE REST OF THE CODE
// THIS FILE IS MERELY MEANT AS MY REFERENCE FOR AN ALTERNATIVE APPROACH FOR THE LOGIC OF FILTERING CART ITEMS AS PER THE PRIMARY CATEGORIES (I.E. FOOD, DRINKS, SNACKS)
// PLEASE VISIT THE CheckoutInfo.js FILE INSTEAD
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { FaTrashAlt } from 'react-icons/fa';
import { REMOVE_ALL_ITEMS_FROM_BASKET } from '../../ReduxStuff/actions';
import '../../CSS/Checkout-Page-Styles/CheckoutInfoStyles.css';

const CheckoutInfo = (props) => {
  const { itemsInBasket, removeItemFromBasket } = props;
  const { t } = useTranslation();

  const history = useHistory();
  React.useEffect(() => {
    if (itemsInBasket.length === 0) {
      history.push('/home');
    }
  });

  const superParentObj = {};
  // eslint-disable-next-line array-callback-return
  itemsInBasket.map((item) => {
    const newItemName = item.superParentName;
    if (Object.keys(superParentObj).indexOf(item.superParentName) === -1) {
      superParentObj[`${newItemName}`] = [item];
    } else {
      superParentObj[`${newItemName}`] = [
        ...superParentObj[`${newItemName}`],
        item,
      ];
    }
  });

  const result = Object.keys(superParentObj).map((key) => {
    return [Number(key), superParentObj[key]];
  });
  console.log(result);

  Object.keys(superParentObj).forEach((foodItem) => {
    console.log(superParentObj[`${foodItem}`].length);
    console.log(foodItem);
  });

  return (
    <div className="checkout-info-container">
      {Object.keys(superParentObj).map((foodItem, index) => {
        return (
          // eslint-disable-next-line react/no-array-index-key
          <div key={index} className="checkout-info-list-main-container">
            <h2>
              {foodItem} (<span>{superParentObj[`${foodItem}`].length}</span>)
            </h2>
            {superParentObj[`${foodItem}`].map((item) => {
              const {
                mainNameOfProduct,
                currentOrderId,
                radioBtnValue,
                checkboxValue,
                counter,
              } = item;

              const checkboxPriceTotal = checkboxValue
                .map((checkboxItem) => {
                  return checkboxItem.price;
                })
                .reduce((a, b) => {
                  return a + b;
                }, 0);

              const totalPriceOfCurrentBasketItem =
                (radioBtnValue.variantPrice + checkboxPriceTotal) * counter;

              return (
                <div
                  key={currentOrderId}
                  className="checkout-info-list-container"
                >
                  <div className="checkout-info-list-name-and-quantity">
                    <h3>
                      <span>{counter}</span> x {mainNameOfProduct}
                    </h3>
                    <p>
                      {radioBtnValue.variantName}
                      {checkboxValue.map((singleCheckboxItem) => {
                        return `, ${singleCheckboxItem.name}`;
                      })}
                    </p>
                  </div>
                  <div className="checkout-info-list-price-and-delete-icon">
                    <h3>£{totalPriceOfCurrentBasketItem.toFixed(2)}</h3>
                    <FaTrashAlt
                      className="trash-icon"
                      onClick={() => {
                        removeItemFromBasket(currentOrderId);
                      }}
                    />
                  </div>
                </div>
              );
            })}
          </div>
        );
      })}
      <div className="separator-line" />
      <label htmlFor="add-notes" className="add-notes-label">
        {t('checkoutInfo.notes')}
      </label>
      <textarea
        className="add-notes-textarea"
        name="add-notes"
        id="add-notes"
        cols="30"
        rows="5"
      />
      <div className="separator-line" />
      <h1 className="table-number">
        {t('checkoutInfo.table')} <span> 32</span>
      </h1>
    </div>
  );
};

const mapStateToProps = (state) => {
  const { itemsInBasket } = state.main;
  return {
    itemsInBasket,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { name, price } = ownProps;
  let { variants } = ownProps;

  if (variants === undefined) {
    variants = [{ name, price }];
  }
  return {
    removeItemFromBasket: (currentOrderIdToBeDeleted) => {
      dispatch({
        type: REMOVE_ALL_ITEMS_FROM_BASKET,
        payload: { currentOrderIdToBeDeleted },
      });
    },
  };
};

CheckoutInfo.propTypes = {
  itemsInBasket: PropTypes.arrayOf(
    PropTypes.shape({
      currentOrderId: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired,
      parentId: PropTypes.number.isRequired,
      counter: PropTypes.number.isRequired,
      checkboxValue: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string,
          price: PropTypes.number,
        })
      ),
    })
  ).isRequired,
  removeItemFromBasket: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutInfo);
