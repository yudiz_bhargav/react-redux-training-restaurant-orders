import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import ConfirmOrderModal from '../Modals/ConfirmOrderModal';
import '../../CSS/Checkout-Page-Styles/CheckoutFooterStyles.css';

const CheckoutFooter = ({ totalBasketItems, totalBasketPrice }) => {
  const { t } = useTranslation();
  const [isConfirmOrderModalOpen, setIsConfirmOrderModalOpen] = useState(false);

  return (
    <>
      <button
        className="checkout-footer"
        type="button"
        onClick={() => {
          setIsConfirmOrderModalOpen(true);
        }}
      >
        {t('checkoutFooter.confirm')}
        <h2 className="checkout-footer-heading">
          £{totalBasketPrice.toFixed(2)} / {totalBasketItems}{' '}
          {t('checkoutFooter.items')}
        </h2>
      </button>
      {isConfirmOrderModalOpen && (
        <ConfirmOrderModal toggleModal={setIsConfirmOrderModalOpen} />
      )}
    </>
  );
};

const mapStateToProps = (state) => {
  const { totalBasketItems, totalBasketPrice } = state.main;
  return { totalBasketItems, totalBasketPrice };
};

CheckoutFooter.propTypes = {
  totalBasketItems: PropTypes.number.isRequired,
  totalBasketPrice: PropTypes.number.isRequired,
};

export default connect(mapStateToProps)(CheckoutFooter);
