import React from 'react';
import { useTranslation } from 'react-i18next';

const CheckoutHeader = () => {
  const { t } = useTranslation();

  return (
    <>
      <h1 className="checkout-heading">{t('checkoutHeader.heading')}</h1>
      <div className="checkout-header-text">
        <h4>{t('checkoutHeader.headerTextHead')}</h4>
        <p>{t('checkoutHeader.headerTextP')}</p>
      </div>
    </>
  );
};

export default CheckoutHeader;
