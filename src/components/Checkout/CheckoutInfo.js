/* eslint-disable no-return-assign */
/* eslint-disable react/prop-types */
/* eslint-disable no-plusplus */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { FaTrashAlt } from 'react-icons/fa';
import { REMOVE_ALL_ITEMS_FROM_BASKET } from '../../ReduxStuff/actions';
import '../../CSS/Checkout-Page-Styles/CheckoutInfoStyles.css';

const CheckoutInfo = (props) => {
  const { itemsInBasket, removeItemFromBasket } = props;
  const { t } = useTranslation();

  const history = useHistory();
  React.useEffect(() => {
    if (itemsInBasket.length === 0) {
      history.push('/home');
    }
  });

  const superParentArray = [];

  itemsInBasket.forEach((item) => {
    const filteredSuperParentArray = superParentArray.filter((arr) => {
      return arr[0].superParentName === item.superParentName;
    });
    if (superParentArray.length !== 0) {
      if (filteredSuperParentArray.length === 0) {
        superParentArray.push([item]);
      } else {
        superParentArray.forEach((arr) => {
          if (arr[0].superParentName === item.superParentName) {
            arr.push(item);
          }
        });
      }
    } else {
      superParentArray.push([item]);
    }
  });

  return (
    <div className="checkout-info-container">
      {superParentArray.map((individualArr, index) => {
        return (
          // eslint-disable-next-line react/no-array-index-key
          <div key={index} className="checkout-info-list-main-container">
            <h2>
              {individualArr[0].superParentName} (
              <span>{individualArr.length}</span>)
            </h2>
            {individualArr.map((itemObj) => {
              const {
                mainNameOfProduct,
                currentOrderId,
                radioBtnValue,
                checkboxValue,
                counter,
              } = itemObj;

              const checkboxPriceTotal = checkboxValue
                .map((checkboxItem) => {
                  return checkboxItem.price;
                })
                .reduce((a, b) => {
                  return a + b;
                }, 0);

              const totalPriceOfCurrentBasketItem =
                (radioBtnValue.variantPrice + checkboxPriceTotal) * counter;

              return (
                <div
                  key={currentOrderId}
                  className="checkout-info-list-container"
                >
                  <div className="checkout-info-list-name-and-quantity">
                    <h3>
                      <span>{counter}</span> x {mainNameOfProduct}
                    </h3>
                    <p>
                      {radioBtnValue.variantName}
                      {checkboxValue.map((singleCheckboxItem) => {
                        return `, ${singleCheckboxItem.name}`;
                      })}
                    </p>
                  </div>
                  <div className="checkout-info-list-price-and-delete-icon">
                    <h3>£{totalPriceOfCurrentBasketItem.toFixed(2)}</h3>
                    <FaTrashAlt
                      className="trash-icon"
                      onClick={() => {
                        removeItemFromBasket(currentOrderId);
                      }}
                    />
                  </div>
                </div>
              );
            })}
          </div>
        );
      })}
      <div className="separator-line" />
      <label htmlFor="add-notes" className="add-notes-label">
        {t('checkoutInfo.notes')}
      </label>
      <textarea
        className="add-notes-textarea"
        name="add-notes"
        id="add-notes"
        cols="30"
        rows="5"
      />
      <div className="separator-line" />
      <h1 className="table-number">
        {t('checkoutInfo.table')} <span> 32</span>
      </h1>
    </div>
  );
};

const mapStateToProps = (state) => {
  const { itemsInBasket } = state.main;
  return {
    itemsInBasket,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { name, price } = ownProps;
  let { variants } = ownProps;

  if (variants === undefined) {
    variants = [{ name, price }];
  }
  return {
    removeItemFromBasket: (currentOrderIdToBeDeleted) => {
      dispatch({
        type: REMOVE_ALL_ITEMS_FROM_BASKET,
        payload: { currentOrderIdToBeDeleted },
      });
    },
  };
};

CheckoutInfo.propTypes = {
  itemsInBasket: PropTypes.arrayOf(
    PropTypes.shape({
      currentOrderId: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired,
      parentId: PropTypes.number.isRequired,
      counter: PropTypes.number.isRequired,
      checkboxValue: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string,
          price: PropTypes.number,
        })
      ),
    })
  ).isRequired,
  removeItemFromBasket: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutInfo);
