import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { CLEAR_THE_ENTIRE_CART } from '../../ReduxStuff/actions';
import beerImg from '../../assets/beer2x.png';
import '../../CSS/Checkout-Page-Styles/Checkout-Page-Modal-Styles/CheersModalStyles.css';

const CheersModal = ({ clearTheEntireCart }) => {
  const { t } = useTranslation();

  return (
    <div className="cheers-modal-background">
      <div className="cheers-modal-container">
        <h1>{t('cheersModal.cheers')}</h1>
        <img src={beerImg} alt="" />
        <Link
          className="cheers-link"
          to="/home"
          onClick={() => {
            clearTheEntireCart();
          }}
        >
          {t('cheersModal.home')}
        </Link>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    clearTheEntireCart: () => {
      dispatch({ type: CLEAR_THE_ENTIRE_CART });
    },
  };
};

CheersModal.propTypes = {
  clearTheEntireCart: PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(CheersModal);
