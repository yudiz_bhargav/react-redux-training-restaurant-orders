import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import '../../CSS/Home-Page-Styles/Home-Page-Modal-Styles/SubMenuItemsModalFooterStyles.css';

import { ADD_ALL_ITEMS_TO_BASKET } from '../../ReduxStuff/actions';

const SubMenuItemsModalFooter = (props) => {
  const { toggleModal, addAllItemsToBasket } = props;
  const { t } = useTranslation();
  const [totalBasketItemsCounter, setTotalBasketItemCounter] = useState(1);

  const counterAddClickHandler = () => {
    setTotalBasketItemCounter(totalBasketItemsCounter + 1);
  };
  const counterSubtractClickHandler = () => {
    if (totalBasketItemsCounter - 1 > 0) {
      setTotalBasketItemCounter(totalBasketItemsCounter - 1);
    } else {
      setTotalBasketItemCounter(1);
    }
  };

  return (
    <>
      <div className="sub-menu-items-modal-items-amount-counter-container">
        <button
          type="button"
          className="sub-menu-items-modal-items-amount-btn"
          onClick={counterSubtractClickHandler}
        >
          -
        </button>
        <h1 className="sub-menu-items-modal-items-amount">
          {totalBasketItemsCounter}
        </h1>
        <button
          type="button"
          className="sub-menu-items-modal-items-amount-btn"
          onClick={counterAddClickHandler}
        >
          +
        </button>
      </div>
      <div className="sub-menu-items-modal-add-to-order-btn-container">
        <button
          type="button"
          className="sub-menu-items-modal-add-to-order-btn"
          onClick={() => {
            toggleModal(false);
            addAllItemsToBasket(totalBasketItemsCounter);
          }}
        >
          {t('homeModal.footer')}
        </button>
      </div>
    </>
  );
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { id, parentId, name, mainNameOfProduct, price } = ownProps;

  const currentOrderId = new Date().getTime().toString();

  let { radioBtnValue, checkboxValue } = ownProps;
  if (Object.keys(radioBtnValue).length === 0) {
    const newRadioBtnValue = {
      variantName: name,
      variantPrice: price,
    };
    radioBtnValue = newRadioBtnValue;
  }
  const filteredCheckboxValue = checkboxValue.filter((val) => {
    return val.name !== '';
  });
  checkboxValue = filteredCheckboxValue;

  return {
    addAllItemsToBasket: (counter) => {
      dispatch({
        type: ADD_ALL_ITEMS_TO_BASKET,
        payload: {
          currentOrderId,
          id,
          mainNameOfProduct,
          parentId,
          radioBtnValue,
          checkboxValue,
          counter,
        },
      });
    },
  };
};

SubMenuItemsModalFooter.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  radioBtnValue: PropTypes.shape({
    name: PropTypes.string,
    price: PropTypes.number,
  }),
  addAllItemsToBasket: PropTypes.func.isRequired,
};

SubMenuItemsModalFooter.defaultProps = {
  radioBtnValue: {},
};

export default connect(null, mapDispatchToProps)(SubMenuItemsModalFooter);
