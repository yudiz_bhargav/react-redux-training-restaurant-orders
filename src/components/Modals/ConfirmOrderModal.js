import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { BiLike } from 'react-icons/bi';
import CheersModal from './CheersModal';
import '../../CSS/Checkout-Page-Styles/Checkout-Page-Modal-Styles/ConfirmOrderModalStyles.css';

const ConfirmOrderModal = ({ toggleModal }) => {
  const { t } = useTranslation();
  const [isCheersModalOpen, setIsCheersModalOpen] = React.useState(false);

  if (isCheersModalOpen) {
    return <CheersModal />;
  }

  return (
    <div className="confirm-order-modal-background">
      <div className="confirm-order-modal-container">
        <h1 className="confirm-order-modal-heading">
          {t('confirmModal.heading')}
        </h1>
        <BiLike className="like-icon" />
        <p>{t('confirmModal.text')}</p>
        <div className="confirm-order-modal-button-container">
          <button
            className="cancel-btn"
            type="button"
            onClick={() => {
              toggleModal(false);
            }}
          >
            {t('confirmModal.cancel')}
          </button>
          <button
            className="place-order-btn"
            type="button"
            onClick={() => {
              setIsCheersModalOpen(true);
            }}
          >
            {t('confirmModal.order')}
          </button>
        </div>
      </div>
    </div>
  );
};

ConfirmOrderModal.propTypes = {
  toggleModal: PropTypes.func.isRequired,
};

export default ConfirmOrderModal;
