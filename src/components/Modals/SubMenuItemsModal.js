/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/no-array-index-key */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import SubMenuItemsModalFooter from './SubMenuItemsModalFooter';
import '../../CSS/Home-Page-Styles/Home-Page-Modal-Styles/SubMenuItemsModalStyles.css';

const SubMenuItemsModal = (props) => {
  const { toggleModal, id, parentId, name, price, variants, extras } = props;
  const { t } = useTranslation();
  const mainNameOfProduct = name;
  const [radioBtnValue, setRadioBtnValue] = useState({});
  const [checkboxValue, setCheckboxValue] = useState([{ name: '', price: 0 }]);

  // eslint-disable-next-line no-undef
  const radioBtn = document.querySelectorAll('.radio-btn');
  radioBtn.forEach((btn) => {
    if (btn.checked === true) {
      return btn.parentElement.classList.add(
        'sub-menu-items-modal-primary-options-container-active'
      );
    }
    return btn.parentElement.classList.remove(
      'sub-menu-items-modal-primary-options-container-active'
    );
  });

  return (
    <div className="modal-background">
      <div className="sub-menu-items-modal-container">
        <div className="sub-menu-items-modal-heading-and-close-btn-container">
          <h1 className="sub-menu-items-modal-heading">{name}</h1>
          <button
            className="sub-menu-items-modal-close-btn"
            type="button"
            onClick={() => {
              toggleModal(false);
            }}
          >
            X
          </button>
        </div>
        <div className="sub-menu-items-modal-separator-line" />
        <h2 className="sub-menu-items-modal-primary-options-heading">
          {t('homeModal.primaryOptionsHeading')}
        </h2>
        <ul>
          {variants.length > 0 ? (
            variants.map((variant, index) => {
              return (
                <li
                  key={index}
                  className="sub-menu-items-modal-primary-options"
                >
                  <div className="sub-menu-items-modal-primary-options-container">
                    <input
                      type="radio"
                      className="radio-btn"
                      name="nonZeroExtras"
                      value={radioBtnValue}
                      onChange={() => {
                        setRadioBtnValue((prevVal) => {
                          return {
                            ...prevVal,
                            variantName: variant.name,
                            variantPrice: variant.price,
                          };
                        });
                      }}
                    />
                    <h3 className="sub-menu-items-modal-primary-options-container-variant-name">
                      {variant.name}
                    </h3>
                    <h4>£{variant.price}</h4>
                  </div>
                </li>
              );
            })
          ) : (
            <div className="sub-menu-items-modal-primary-options">
              <div className="sub-menu-items-modal-primary-options-container">
                <input
                  type="radio"
                  className="radio-btn"
                  name={name}
                  value={radioBtnValue}
                  onChange={() => {
                    setRadioBtnValue((prevVal) => {
                      return {
                        ...prevVal,
                        variantName: name,
                        variantPrice: price,
                      };
                    });
                  }}
                />
                <h3 className="sub-menu-items-modal-primary-options-container-variant-name">
                  {name}
                </h3>
                <h4>£{price}</h4>
              </div>
            </div>
          )}
        </ul>
        <div className="sub-menu-items-modal-separator-line" />
        {extras.length > 0 ? (
          <>
            <h2 className="sub-menu-items-modal-secondary-options-heading">
              {t('homeModal.secondaryOptionsHeading')}
            </h2>
            <ul>
              {extras.map((extraItem, index) => {
                return (
                  <li
                    key={index}
                    className="sub-menu-items-modal-secondary-options"
                  >
                    <h3>
                      {extraItem.name} (<span>+ £{extraItem.price}</span>)
                    </h3>
                    <input
                      type="checkbox"
                      className="checkbox"
                      onChange={() => {
                        setCheckboxValue((prevValue) => {
                          const findMethod = prevValue.find((val) => {
                            return val.name === extraItem.name;
                          });
                          if (findMethod === undefined) {
                            return [
                              ...prevValue,
                              { name: extraItem.name, price: extraItem.price },
                            ];
                          }
                          return prevValue.filter((val) => {
                            return val.name !== extraItem.name;
                          });
                        });
                      }}
                    />
                  </li>
                );
              })}
            </ul>
            <div className="sub-menu-items-modal-separator-line" />
          </>
        ) : (
          <></>
        )}
        <SubMenuItemsModalFooter
          toggleModal={toggleModal}
          id={id}
          parentId={parentId}
          name={name}
          mainNameOfProduct={mainNameOfProduct}
          price={price}
          radioBtnValue={radioBtnValue}
          checkboxValue={checkboxValue}
        />
      </div>
    </div>
  );
};

SubMenuItemsModal.propTypes = {
  toggleModal: PropTypes.func.isRequired,

  id: PropTypes.number.isRequired,
  parentId: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  variants: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      price: PropTypes.number,
    })
  ),
  extras: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      price: PropTypes.number,
    })
  ),
};

SubMenuItemsModal.defaultProps = {
  variants: [],
  extras: [],
};

export default SubMenuItemsModal;
