/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import CheckoutHeader from '../Components/Checkout/CheckoutHeader';
// import CheckoutInfo from '../Components/Checkout/CheckoutInfo';
import CheckoutInfo from '../Components/Checkout/CheckoutInfo';
import CheckoutFooter from '../Components/Checkout/CheckoutFooter';
import '../CSS/Checkout-Page-Styles/CheckoutPageStyles.css';

const Checkout = ({ itemsInBasket }) => {
  const history = useHistory();
  React.useEffect(() => {
    // eslint-disable-next-line no-undef

    if (itemsInBasket.length === 0) {
      history.push('/home');
    }
  }, []);

  return (
    <div className="checkout-page-container">
      <CheckoutHeader />
      <CheckoutInfo />
      <CheckoutFooter />
    </div>
  );
};

const mapStateToProps = (state) => {
  const { itemsInBasket } = state.main;
  return { itemsInBasket };
};

Checkout.propTypes = {
  itemsInBasket: PropTypes.arrayOf(PropTypes.object),
};

Checkout.defaultProps = {
  itemsInBasket: [],
};

export default connect(mapStateToProps)(Checkout);
