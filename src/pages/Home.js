import React from 'react';
import HomeFooter from '../Components/Home/HomeFooter';
import HomeHeader from '../Components/Home/HomeHeader';
import MenuCategories from '../Components/Home/MenuCategories';
import '../CSS/Home-Page-Styles/HomePageStyles.css';

const Home = () => {
  return (
    <div className="home-component-container">
      <HomeHeader />
      <MenuCategories />
      <HomeFooter />
    </div>
  );
};

export default Home;
