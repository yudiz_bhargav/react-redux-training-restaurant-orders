import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import '../CSS/Error-Page-Styles/Error-Page-Styles.css';

const ErrorPage = () => {
  const history = useHistory();

  // eslint-disable-next-line no-undef
  if (window.location.href === 'http://localhost:3000/') {
    history.push('/home/DRINKS-1/Beers-7');
  }

  return (
    <div className="error-page-container">
      <h1>PAGE YOU ARE TRYING TO REACH DOES NOT EXIST!</h1>
      <Link to="/home" className="error-page-link">
        Go Back Home
      </Link>
    </div>
  );
};

export default ErrorPage;
