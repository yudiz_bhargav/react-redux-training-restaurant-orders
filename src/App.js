import React, { Suspense } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './Pages/Home';
import Checkout from './Pages/Checkout';
import Error from './Pages/ErrorPage';
import './CSS/App.css';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/home" component={Home} />
        <Route path="/checkout" exact component={Checkout} />
        <Route path="*" component={Error} />
      </Switch>
    </BrowserRouter>
  );
}

export default function WrappedApp() {
  return (
    <Suspense fallback="...is loading">
      <App />
    </Suspense>
  );
}
