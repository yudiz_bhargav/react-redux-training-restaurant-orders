import * as ACTIONS from '../actions';
import Categories from '../../Data/Categories.json';
import Products from '../../Data/products.json';

const { products } = Products;
const productsForReset = products;

const { categories } = Categories;
const categoriesForReset = categories;

const primaryCategories = categories.filter((category) => {
  return category.parent === null;
});
const subCategories = categories.filter((category) => {
  return category.parent !== null;
});

const initialStore = {
  primaryCategories,
  subCategories,
  products,

  itemsInBasket: [],
  totalBasketPrice: 0,
  totalBasketItems: 0,
};

const mainReducer = (state = initialStore, action) => {
  switch (action.type) {
    case ACTIONS.SET_SUB_CATEGORIES: {
      let filteredSubCategories = state.subCategories.filter((category) => {
        return category.parent === action.payload.primaryCategoryId;
      });
      if (filteredSubCategories.length === 0) {
        const subCategoriesReset = categoriesForReset.filter((category) => {
          return category.parent !== null;
        });
        filteredSubCategories = subCategoriesReset.filter((category) => {
          return category.parent === action.payload.primaryCategoryId;
        });
      }
      return { ...state, subCategories: filteredSubCategories };
    }
    case ACTIONS.SET_PRODUCTS: {
      let filteredProducts = state.products.filter((product) => {
        return product.parentId === action.payload.subMenuCategoryId;
      });
      if (filteredProducts.length === 0) {
        filteredProducts = productsForReset.filter((product) => {
          return product.parentId === action.payload.subMenuCategoryId;
        });
      }
      return { ...state, products: filteredProducts };
    }
    case ACTIONS.ADD_ALL_ITEMS_TO_BASKET: {
      const {
        currentOrderId,
        id,
        mainNameOfProduct,
        parentId,
        radioBtnValue,
        checkboxValue,
        counter,
      } = action.payload;

      const parentIdOfProduct = state.subCategories.filter((category) => {
        return category.id === parentId;
      })[0].parent;

      const parentOfSubCategory = state.primaryCategories.filter((primary) => {
        return primary.id === parentIdOfProduct;
      })[0];
      const superParentName = parentOfSubCategory.name;
      const newOrderForBasket = {
        currentOrderId,
        id,
        mainNameOfProduct,
        superParentName,
        parentId,
        radioBtnValue,
        checkboxValue,
        counter,
      };
      return {
        ...state,
        itemsInBasket: [...state.itemsInBasket, newOrderForBasket],
      };
    }

    case ACTIONS.REMOVE_ALL_ITEMS_FROM_BASKET: {
      let grandTotal = 0;
      state.itemsInBasket.forEach((basketItem) => {
        if (
          basketItem.currentOrderId === action.payload.currentOrderIdToBeDeleted
        ) {
          let totalCheckboxPrice = 0;
          const { radioBtnValue, checkboxValue, counter } = basketItem;
          checkboxValue.map((singleCheckboxItem) => {
            // eslint-disable-next-line no-return-assign
            return (totalCheckboxPrice += singleCheckboxItem.price);
          });
          // eslint-disable-next-line no-return-assign
          return (grandTotal +=
            (radioBtnValue.variantPrice + totalCheckboxPrice) * counter);
        }
        return grandTotal;
      });

      const filteredItemsInBasket = state.itemsInBasket.filter((basketItem) => {
        return (
          basketItem.currentOrderId !== action.payload.currentOrderIdToBeDeleted
        );
      });
      return {
        ...state,
        itemsInBasket: filteredItemsInBasket,
        totalBasketPrice: state.totalBasketPrice - grandTotal,
        totalBasketItems: state.totalBasketItems - 1,
      };
    }

    case ACTIONS.CLEAR_THE_ENTIRE_CART: {
      return {
        ...state,
        itemsInBasket: [],
        totalBasketPrice: 0,
        totalBasketItems: 0,
      };
    }

    case ACTIONS.SET_BASKET_TOTAL: {
      return {
        ...state,
        totalBasketPrice: state.totalBasketPrice + action.payload.totalPrice,
      };
    }

    case ACTIONS.SET_NUMBER_OF_BASKET_ITEMS: {
      return {
        ...state,
        totalBasketItems: state.totalBasketItems + action.payload.totalItems,
      };
    }

    default:
      return state;
  }
};

export default mainReducer;
