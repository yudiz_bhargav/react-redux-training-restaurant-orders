import * as ACTIONS from './actions';

export const setSubCategories = (primaryCategoryId) => {
  return {
    type: ACTIONS.SET_SUB_CATEGORIES,
    payload: { primaryCategoryId },
  };
};

export const setProducts = (subMenuCategoryId) => {
  return {
    type: ACTIONS.SET_PRODUCTS,
    payload: { subMenuCategoryId },
  };
};

// export const addAllItemsToBasket = () => {};

export const setBasketTotalPrice = (totalPrice) => {
  return {
    type: ACTIONS.SET_BASKET_TOTAL,
    payload: { totalPrice },
  };
};

export const setNumberOfBasketItems = (totalItems) => {
  return {
    type: ACTIONS.SET_NUMBER_OF_BASKET_ITEMS,
    payload: { totalItems },
  };
};

// export const removeItemsFromBasket = (currentOrderIdToBeDeleted) => {};

// export const clearTheEntireCart = () => {};
